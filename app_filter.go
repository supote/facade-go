package facade

import (
	"github.com/astaxie/beego/cache"
	"github.com/astaxie/beego/context"
)

type AppFilter interface {
	VersionFilter(versionList []string) func(ctx *context.Context)
	BotFilter() func(ctx *context.Context)
	AppKeyFilter(beegoCache cache.Cache) func(ctx *context.Context)
	CredentialFilter() func(ctx *context.Context)
    AppKeyAuthFilter(authKey string) func(ctx *context.Context)
    AppExpireFilter(start int64, expire int64) func(ctx *context.Context)
}
