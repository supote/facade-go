package facade

import (
	"time"
)

type SyncHandler interface {
	Debug(debug bool)
	GetConfig() *SyncConfig
	Source(v interface{}) SyncHandler
	Target(v interface{}) SyncHandler
	FetchSource(m func(adapter Adapter, config *SyncConfig, input interface{}) (int, []interface{})) SyncHandler
	UpdateJournalSource(m func(adapter Adapter, config *SyncConfig, input interface{}, data interface{}, dataModel interface{}) error) SyncHandler
	BindDataModel(m func(adapter Adapter, config *SyncConfig, data interface{}, dataModel interface{}) interface{}) SyncHandler
	ClearTarget(m func(adapter Adapter, config *SyncConfig, input interface{}) error) SyncHandler
	DeleteTarget(m func(adapter Adapter, config *SyncConfig, input interface{}, data interface{}, dataModel interface{}) error) SyncHandler
	HookBeforeTarget(task string, m func(adapter Adapter, config *SyncConfig, input interface{}, data interface{}, dataModel interface{}) (interface{}, error)) SyncHandler
	HookAfterTarget(task string, m func(adapter Adapter, config *SyncConfig, input interface{}, data interface{}, dataModel interface{}) (interface{}, error)) SyncHandler
	ProcessTarget(m func(adapter Adapter, config *SyncConfig, input interface{}, data interface{}, dataModel interface{}) (interface{}, error)) SyncHandler
	PublishTarget(m func(adapter Adapter, config *SyncConfig, input interface{}, data interface{}, dataModel interface{}) error) SyncHandler
	UpdateJournalTarget(m func(adapter Adapter, config *SyncConfig, input interface{}, data interface{}, dataModel interface{}) error) SyncHandler
	SetInputData(input interface{})
	//SourceCSVFileName(path string, firstRowIsHeader bool)
	//SourceCSVFilePath(path string, firstRowIsHeader bool)

	// Database (Source) -> Database (Target)
	// SourceDB()
	// TargetDB()
	// CDC is the source journal
	// 1. Clear all data from target database.
	// 2. Fetch all CDC data from source database
	// 3. Get fields data from source.
	// 4. Delete sync item from target database (CDC).
	// 5. If the create target is success, it will deleted from the journal
	Sync() (error, int, int, int)

	// DB -> Elastic, NoSQL
	/*
		SyncType1(fetchJournal func(OrmHandler, interface{}) (int, []interface{}),
			clearNoSQLData func(CRUDDocumentStore, interface{}) error,
			clearElasticData func(ElasticHandler, interface{}) error,
			bindingDatadataModel func(OrmHandler, interface{}, map[string]interface{}) interface{},
			upsertElastic func(ElasticHandler, string, string, interface{}, interface{}) (bool, error),
			upsertNoSQL func(CRUDDocumentStore, interface{}, interface{}) bool,
			updateJournal func(OrmHandler, interface{}, map[string]interface{}) bool)
	*/
	// Database (Source) -> DB (Target)

	//SyncType10(csvColumndataModel func() []CSVModel,
	//	getTargetDBColumn func() (string, []string)) bool

	// DB -> NoSQL
	// DB -> Elastic
	// NoSQL -> Elastic
	// NoSQL -> DB
	// Elastic -> NoSQL
	// Elastic -> DB
	// API -> NoSQL : 1, 2, 3
	// NoSQL -> API : 3, 2 ,1
	// API -> File System
	// API -> Zip
	// API -> FTP(s)

	//SetSourceTransport(connector interface{})
	//SetTargetTransport(connector interface{})

	// Export
	/*
		Export(fetchSource func(connector interface{}, input_data interface{}) (int, []interface{}),
			updateSourceJournal func(connector interface{}, input_data interface{}) (bool),
			deleteTarget func(connector interface{}, input_data interface{})
			createTarget func(connector interface{}, input_data interface{})
	*/
	// Import

	/*
		Sync(isCompare Bool,
			compareSourceWithTarget func(connector interface{}, input_data interface{}) err,
			fetchSource func(connector interface{}, input_data interface{}) (int, []interface{}),
			fetchJournal func(connector interface{}, input_data interface{}) (int, []interface{}),
			clearSource func(connector interface{}, input_data interface{}) error,
			clearTarget func(connector interface{}, input_data interface{}) error,
			deleteTarget func(connector interface{}, input_data interface{}, data map[string]interface{}) bool,
			createTarget func(connector interface{}, input_data interface{}, data map[string]interface{}) (int, error),
			deleteJournal func(connector interface{}, input_data interface{}, data map[string]interface{}) bool) (int, int)
	*/
}

type CSVdataModel struct {
	Index                int
	Heading              string
	ColumnType           string // string, float64, int64
	DefaultValue         interface{}
	ConvertBlank         bool
	ConvertSingleQuote   bool
	ConvertHyphen        bool
	ConvertComma         bool
	TrimWhiteSpace       bool
	NullValueTextToEmpty bool
}

type SyncConfig struct {
	IsTrackingState         bool
	ProcessId               string
	TrackingColumn          string
	SqlLastTimeValue        time.Time
	CurrentTimeValue        time.Time
	SqlLastValue            int
	TrackingColumnType      int  // 0 is numeric , 1 is timestamp
	UseColumnValue          bool // When set to false, :sql_last_value reflects the last time the query was executed. When set to true, uses the defined tracking_column value as the :sql_last_value.
	Topic                   string
	VfsPath                 string
	BulkSize                int
	ElasticAutoDisableIndex bool
	ElasticIndex            string
	ElasticDocType          string
	DelayUpdateBulk         time.Duration
	SkippedOnError          bool
	CsvInputFile            string
	CsvOutputFile           string
	IsFirstRowHeader        bool
}

type TimestampTask struct {
	Name           string
	StartTimestamp time.Time
	EndTimestamp   time.Time
}
