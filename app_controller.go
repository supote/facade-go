package facade

type AppController interface {
	Handler() Handler
	Connector() Connector
	Context() AppContext
}
