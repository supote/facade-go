package facade

type Quote interface {
	Quote(s string) string
	AppendQuote(dst []byte, s string) []byte
	QuoteToASCII(s string) string
	AppendQuoteToASCII(dst []byte, s string) []byte
	QuoteToGraphic(s string) string
	AppendQuoteToGraphic(dst []byte, s string) []byte
	QuoteRune(r rune) string
	AppendQuoteRune(dst []byte, r rune) []byte
	QuoteRuneToASCII(r rune) string
	AppendQuoteRuneToASCII(dst []byte, r rune) []byte
	QuoteRuneToGraphic(r rune) string
	AppendQuoteRuneToGraphic(dst []byte, r rune) []byte
	CanBackquote(s string) bool
	UnquoteChar(s string, quote byte) (value rune, multibyte bool, tail string, err error)
	Unquote(s string) (string, error)
	IsPrint(r rune) bool
	IsGraphic(r rune) bool
}
