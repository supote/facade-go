package facade

import (
	"github.com/astaxie/beego/cache"
	"github.com/jmoiron/sqlx"
)

type Connector interface {
	Kafka() Kafka
	Mqtt(key string, clientId string) MqttClient
	RedisCache(db int) CacheHandler
	Elastic() ElasticHandler
	NoSql(db string) CRUDDocumentStore
	HttpClient() HttpClient
	ConnectKafka(broker string) Kafka
	SetOrm(key string, orm OrmHandler) Connector
	GetORMHandler(db *sqlx.DB) OrmHandler
	GetOrm(key string) OrmHandler
	BeegoCache() cache.Cache
	// Get RPC cache connector.
	RpcCache() RpcCacheConnector
	GetCacheRpc(username string, token_id string) OdooCredential
	// Get RPC connector.
	Rpc() RpcConnector
	Csv() CsvHandler
    LDAP(config *LDAPConfig) LDAPClient
	IsValidSession(username string, token_id string) bool
	InvalidateSession(token_id string)
}
