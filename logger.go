package facade

type Logger interface {
	Debug(v ...interface{})
	DebugFormat(format string, v ...interface{})
	Info(v ...interface{})
	InfoFormat(format string, v ...interface{})
}
